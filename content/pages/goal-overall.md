Title: Goals - Overall 
Slug: goal-overall
Summary:
Modified: 2018-08-02

[TOC]

If you haven't already, please read [the "Announcing Tildes" blog post](https://blog.tildes.net/announcing-tildes) first. It goes over many of the major goals of the site, and this page mostly covers other aspects that aren't included in that post.

## The Golden Rule

There are many variants of the ["golden rule"](https://en.wikipedia.org/wiki/Golden_Rule), but the base idea is that you should act towards others as you'd like them to act towards you. That philosophy applies to various aspects of how I'm approaching building Tildes&mdash;in the end, I'm trying to build the community site that I wish existed, one that treats its users the way they want to be treated.

For example, [having low tolerance for people that consistently make others' experience worse](https://blog.tildes.net/announcing-tildes#limited-tolerance-especially-for-assholes). Nobody (except trolls) hopes to get abuse in response to their posts, so there's no reason to allow that kind of behavior. If people treat each other in good faith and [apply charitable interpretations](https://en.wikipedia.org/wiki/Principle_of_charity), everyone's experience improves.

This sort of approach can also apply to decisions related to site mechanics and features. For example, when a feature has a privacy implication, we should consider how we would want our own data to be treated. If the idea of another site collecting similar data would make us nervous, we should try to figure out a way to adjust the feature to reduce or remove that anxiety.

## Communicate openly and honestly

Running a community or platform that people enjoy being a part of is largely about trust. Without trust, every action or change is viewed with suspicion, as people try to figure out "the real reason" that it's being done.

Trust is often lost due to a lack of communication, or a history (or perception) of being deceptive. It's not especially difficult to prevent this from happening, but it requires a willingness to communicate regularly, explain the true reasons behind what's being done, and solicit (and actually be willing to listen to) feedback.

Part of this is avoiding "PR-speak", where companies utilize deliberately abstruse terminology as a mechanism for disseminating information to segments of their stakeholders. I'll always try to communicate in plain language&mdash;if I need to shut something down I'll tell you it's being shut down, not ["sunset"](https://ig.ft.com/sites/guffipedia/sunset/). The [Privacy Policy](/pol-privacy-policy) and [Terms of Service](/pol-terms-of-use) for Tildes were written in this way as well, with as little legalese as possible.

## Trust people, but punish abusers

The large majority of users on a site generally behave in good faith, and are only interested in legitimately participating and contributing. However, there is always a group of users actively trying to undermine others, and even though they are usually a tiny minority, sites often have to build in such a way to prevent these bad-faith users from being able to do much damage.

This tends to mean that many potentially powerful tools cannot be added to the site, since malicious use of them would be too dangerous. Instead of restricting capabilities by needing to design around the worst way any tool could be used, Tildes will default to trusting users to behave in good faith, and punish people that take advantage of that trust. Punishments may involve losing access to certain tools or capabilities, or being banned from communities or the site as a whole.

## Recognize that users are people, not just metrics

In his talk, ["Is Anything Worth Maximizing?"](http://nxhx.org/maximizing/), Joe Edelman discusses the difference between making decisions based on metrics compared to basing them on the users' reasons for visiting the site. Too often, sites focus on increasing their raw numbers (pageviews, time on site, etc.) instead of thinking about *why* the users are there and trying to improve that experience. This is generally because the site's own goals don't align with the users'&mdash;for example, relying on advertising for revenue means that the site wants to show users as many ads as possible, while users would prefer to see none at all.

Because Tildes has been organized specifically to cater to its users' interests, this type of conflict isn't present, and we can focus solely on improving the user experience instead of obsessing over metrics that don't necessarily reflect how well the site serves its users.

As a specific example, many sites are constantly performing thousands of experiments ("A/B tests") on random sets of users to see how changes to the interface or behavior might affect their metrics. This can be a frustrating experience as a user, since elements move around, behave differently, or even disappear entirely from one day to the next. This is exactly what I want to avoid&mdash;regularly annoying users and degrading their experience solely because of an obsession with metrics.

## Let users make their own decisions about what they want to see

Another recent trend has to been to rely heavily on machine-learning and "personalization algorithms" to determine what you see when you visit a site. These can work well, but they also often jump to wildly wrong conclusions. To quote [one of my favorite tweets on the topic](https://twitter.com/acarboni/status/976545648391553024):

> Me: *watches a single YouTube tutorial so I can fix my door hinge* YouTube: WHAT'S UP, HINGE-LOVER? HERE ARE THE TOP 1000 VIDEOS FROM THE HINGER COMMUNITY THIS WEEK. CHECK OUT THIS TRENDING HINGE CONTENT FROM ENGAGING HINGEFLUENCERS

These algorithms have largely replaced predictable and chronological feeds, instead [trying to addict users by turning the experience into a slot machine](https://medium.com/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3#.sydedohu0) where we're never sure if we're "done" or what content we're going to be given.

On Tildes, I want to stick to predictable ways to view content, along with using additional information (such as metadata and [tags](https://docs.tildes.net/feat-mechanics-design#topic-tags-filtering)) to give users flexible methods of deciding for themselves what they want to see (and not see). Once again, since Tildes doesn't need to prioritize growth or showing ads, it can stay away from manipulative mechanics and focus on just helping users find what they want as easily as possible.

## In-depth content (primarily text-based) is the most important

This includes linking to articles on other sites, posting text topics on Tildes itself, and the comment discussions. In general, any changes to the site that will cause "shallower" content to gain an advantage should be considered very carefully.
