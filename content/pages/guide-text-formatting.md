Title: Guides - Text Formatting
Slug: guide-text-formatting
Summary:
Modified: 2018-08-02

[TOC]

## GitHub Flavored Markdown

Both topic text and comments on Tildes are formatted using the [GitHub Flavored Markdown (GFM) specification](https://github.github.com/gfm/), which is a superset of [CommonMark](http://commonmark.org/), which is itself a spec of [Markdown](https://daringfireball.net/projects/markdown/syntax).

It may sound complicated but it's really not since they all function nearly identically to one another with only some slight variations in syntax meant to address issues with their predecessor and to add more functionality.  So if you're already familiar with Markdown, which many other sites and services use for text formatting, you should generally feel right at home on Tildes.

This guide will attempt to explain the basics in as easily an understandable manner as possible.

### Other references

* [Official CommonMark cheatsheet](http://commonmark.org/help/)
* [10 minute interactive CommonMark tutorial](http://commonmark.org/help/tutorial/)

## Tildes-Specific Syntax

Custom syntax unique to Tildes.

### Linking to Groups and Subgroups

Typing a group name (including the `~` prefix) will automatically link to that group, and adding `.` afterwards followed by a subgroup name will automatically link to that subgroup.

`~tildes` = [~tildes](https://tildes.net/~tildes)  
`~tildes.official` = [~tildes.official](https://tildes.net/~tildes.official)

### Linking to Users

Using the `@` or `/u/` prefix follow by a username will automatically link to that user's profile page.

`@flaque` = [@flaque](https://tildes.net/user/flaque)  
`/u/flaque` = [/u/flaque](https://tildes.net/user/flaque)

## Common Formatting

These common functions should be familiar to anyone who has used Markdown or CommonMark before, however GFM is a slightly more strict spec and adds some additional functionality as well.

### Headings

You can create a heading by starting a line with one or more `#`, followed by a space, and then your desired heading text.  For example, the two previous headings in this document were:  
`## Common Formatting`  
`### Headings`

There are six possible heading levels, each with diminishing font sizes.
```
# h1 - Largest heading
## h2 - Second largest heading
### h3 - Third largest heading
#### h4 - Fourth largest heading
##### h5 - Fifth largest heading
###### h6 - Sixth largest heading
```

### Bold & Italics

By surrounding text in single asterisks you can create italicized text.  
`*italicized text*` = *italicized text*

By using two asterisks on each side you can create bold text.  
`**bold text**` = **bold text**

You can also use underscores in place of asterisks if you prefer.

### Links

You can create links using the following bracket based format:  
`[description text here](https://example.com)` =  [description text here](https://example.com)

If you don't require any description text, you can also just type the URL itself with no special formatting and it will be automatically converted to a link.

### Quotes

You can quote a piece of text by using `>` at the start of a line, and in order for several lines of quotes to be grouped together a `>` is needed on each new line.

```
>**"This is some famous quote."**
>
>-ByThisGuy
```

Which renders similar to this:

>**"You miss 100% of the shots you don't take."**<sub><sub><sub>-wayne gretzky</sub></sub></sub>  
>
>-Michael Scott


### Backslash - The Escape Character

Backslash ` \ ` is an important character in Markdown and all derivative specs as it allows you to "escape" the formatting usages for any character that follows it and force it to be interpreted as plain text.

For example, if you wanted to write some text surrounded by asterisks but not have the text in between them be rendered as italicized or bolded text, you must prefix the asterisks with a backslash.

`\*Text I don't want italicized!\*` = \*Text I don't want italicized!\*  
`\*\*Text I don't want bolded!\*\*` = \*\*Text I don't want bolded!\*\*

This works with most formatting usage characters as well:

`\>This isn't a quote!`  
\>This isn't a quote!

`\[This isn't a description\]\(nor is this a website!\)`  
\[This isn't a description\]\(nor is this a website!\)

Etc...

### Preformatted Text and Code

If you need to put an in-line code segment wrapped within a string of regular text, you can surround it with single backticks.

```
Check out my code! `console.log("hello world")` Isn't it cool?
```

Which renders as:

Check out my code! `console.log("hello world")` Isn't it cool?

---

If you need to write a multi-line block of code, you can use three backticks on separate lines surrounding it.

    ```
    console.log("hello");
    console.log("goodbye");
    ```

Or instead of the triple-backticks, you can also simply indent each line of the code by 4 spaces.  Both of which will render as:

    console.log("hello");
    console.log("goodbye");

### Lists

You can use `*` `-` or `+` at the start of lines, followed by a space and then your desired text to create an unordered (bullet point) list.  Four extra spaces in front of the list symbol on a line will nest it under the previous one.

```
* Lions
* Tigers
* Bears
    * Oh my!
```

Which renders as:

* Lions
* Tigers
* Bears
    * Oh my!

---

You can similarly use numbers followed by a period to make an ordered list and even combine it with unordered list elements as well if you desire.

```
1. Put your right leg in
2. Pull your right leg out
3. Put your right leg in
     + Shake it all about
```

Which renders as:

1. Put your right leg in
2. Pull your right leg out
3. Put your right leg in
    + Shake it all about

### Horizontal rules

You can add a horizontal rule line with `---` which is useful for splitting your comment into sections.

```
**Chapter 1** - *Once upon a time*

---

**Chapter 2** - *The end*
```

Which renders as:

**Chapter 1** - *Once upon a time*

---

**Chapter 2** - *The end*

## HTML Support (limited)

Tildes also allows you to use some HTML directly rather than using the standard markdown syntax. Generally any HTML that could be generated by markdown can also be entered manually as well.

For example, instead of using the `[]()` syntax for links the `a href` HTML element can be used.

`<a href="https://tildes.net">Go to Tildes!</a>` = [Go to Tildes!](https://tildes.net)

And instead of using asterisks for italics and bold, `<i>` and `<b>` can be used instead.

`<i>italics</i>` = <i>italics</i>
`<b>bold</b>` = <b>bold</b>  

Just make sure to close all your HTML elements using `</nameofelement>` or everything written after they are opened will be considered part of them, even on the next lines.

## HTML-Exclusive Formatting

There are also several formatting features that can currently only be created by writing HTML (no markdown syntax is available):

### Strikethrough

Strikethrough text can be created by using the `<del>` element.  

`<del>The crossed-out text</del>` = <del>The crossed-out text</del>.

You can also (optionally) identify the text you're replacing the strikethrough text with using the `<ins>` element.

`I think the event starts at <del>9 PM</del> <ins>10 PM</ins>` = I think the event starts at <del>9 PM</del> <ins>10 PM</ins>

### Superscript and Subscript

You can add superscript and subscript text using the `<sup>` and `<sub>` elements, respectively.

`E=mc<sup>2</sup>` = E=mc<sup>2</sup>  
`H<sub>2</sub>O` = H<sub>2</sub>O

### Tables

You can also create tables using the HTML format.

```
<table>
  <tr><th>Cell 1 header</th><th>Cell 2 header</th></tr>
  <tr><td>1st row, 1st cell</td><td>1st row, 2nd cell</td></tr>
  <tr><td>2nd row, 1st cell</td><td>2nd row, 2nd cell</td></tr>
</table>
```

Renders as:  
<table>
  <tr><th>Cell 1 header</th><th>Cell 2 header</th></tr>
  <tr><td>1st row, 1st cell</td><td>1st row, 2nd cell</td></tr>
  <tr><td>2nd row, 1st cell</td><td>2nd row, 2nd cell</td></tr>
</table>


If you are unfamiliar with the HTML table format or find it difficult to write from scratch, there are many free HTML table generator tools and services available online and you should be able to just copy their HTML output directly into your posts.  Many common spreadsheet programs can also export to HTML tables as well.