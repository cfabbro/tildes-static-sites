Title: Guides - Topic Tagging
Slug: guide-topic-tagging
Summary:
Modified: 2018-08-02

[TOC]

## Basic info/restrictions

In order for users to tag a topic, a field is provided for them in the "Post a new topic" page.  Tags may only be comprised of ASCII letters, numbers, and spaces/underscores (which are synonymous). Some special characters are also allowed; periods which are used as tag hierarchy delimiters, and commas which are used to separate each tag.  There is currently no restriction on the number of tags which can be added to a topic however this may change eventually.  Tags can also be edited after the topic has already been submitted by clicking `tags` on the topic comments section page underneath the topic link or text, and if any edits are made to the tags, they are recorded in the `topic log` shown in the topic info sidebar.

Currently only the user that posted a topic (or a site admin) may add and edit tags for that topic, however the plan is to eventually allow other users to be able to do so as well.  This crowd-sourced tagging mechanic will likely be tied in to the [Trust/Reputation system](/feat-trust-reputation) once that is developed, so that users who contribute and edit tags appropriately can earn trust within that group for doing so, and those who abuse the system can be punished with a loss of trust or even have their tagging privileges revoked.

Similar to groups, tags also support a hierarchy which can aid users in refining their searches and filters. For example, topics tagged with `rock.progressive` will still appear in tag searches for `rock` and should a user desire to filter out progressive rock specifically, or all rock but progressive rock, they may.

## General tagging guidance

Since tags are used for searching and filtering, try to think about how other users would want to find or filter out the topic you're tagging. For example, when tagging a post about a video game, it can be useful to include tags for the platform the game is on, as well as the game's genre.

Tags should generally be plural (when that makes sense). This is easiest to think of as a parallel to sub-groups where, if there was a group for discussing watches, it would probably be called ~hobbies.watches and not ~hobby.watch, so the correct tag for a post about a watch would be `watches` and not `watch`.

Don't add a tag that's the same or very similar to the group that you're posting in. There's no need to add a `music` tag to posts in ~music, or `technology` tag to posts in ~tech.

## Specific tags

There are several tags that have special meaning on the site and are generally used to distinguish common types of posts that some users desire to filter out.

### The "ask" tag

The `ask` tag should be used when the topic's purpose is to request information from other Tildes users (as opposed to the topic itself providing the information). You should also consider adding a second, separate tag after `ask` to specify what sort of request it is. Some of the commonly used ones are:

+ `casual` - For when the goal of the question is to spark casual conversation, rather than definitive answers.
+ `help` - Requests for help with something, where there is likely a definitive correct answer or likely resolution to a problem. A good example of topics that should be tagged `help` are question related to tech support, "how do I do X on my operating system?"
+ `recommendations` - Requests for recommendations or suggestions (of music, books, games, etc.), where there is no definitive or objective answer but mostly just opinions.
+ `survey` - Questions asking about other users' personal choices or situations, such as "What's your favorite X?", "Which Y do you use?", etc.

### Country tags

If the topic is mostly relevant to a particular country (probably most common for posts in ~news), please tag it with the name of that country. Don't use the nationality or other similar terms&mdash;that is, the tag should be `germany` and not `german`.

For the United States of America, use the tag `usa` (not `us`, `united states`, etc.).
