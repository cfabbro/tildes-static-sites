Title: FAQ - Content
Slug: faq-content
Summary:
Modified: 2018-08-01

[TOC]

Answers to commonly asked questions about the content permitted on Tildes that are not addressed in the [Code of Conduct](/pol-code-of-conduct) or ["Content Restrictions"](/pol-terms-of-use#content-restrictions) section of the [Terms of Use](/pol-terms-of-use).

## What is "Fluff" and is it allowed on Tildes?

"Fluff" is content that requires little effort to create, is designed primarily for instant gratification, is often mindlessly consumed and voted on, and has very little potential to spark meaningful, deep and/or interesting discussions about it.  Some examples of fluff are traditional internet memes, gifs and cute animal pictures.

While fluff is not specifically disallowed on Tildes, and there will inevitably be some amount of fluff content on the site, it is generally heavily discouraged.  And since Tildes is not interested in becoming the next imgur or reddit, fluff will always take a back seat to more substantial content. If fluff begins to dominate the site, steps will be taken to ensure that it suffers from severe enough penalties to render it uncompetitive with the more substantive discussions and high-effort contributions.

This is still an evolving discussion but with so many places on the Internet catering to fluff, it was felt there is no compelling reason to do the same on Tildes.

## Self-promotion: Can I submit my own work, project or business to Tildes?

Yes, but don't spam it and don't solely use the site to promote your own material to the exclusion of all other activities.  A user with a personal project they submit when/where relevant is fine, but a business with an account dedicated solely to promoting it on the site is not.

## Does Tildes allow pornography?

No. What exactly qualifies as pornography is something that will require further exploration and discussion before consensus on an acceptable definition is reached, but for now the general rule is that if its sole purpose is sexual gratification then it is considered porn and not allowed on Tildes.  There are many other websites better suited to sharing porn and better equipped to dealing with the myriad of ethical/legal issues and obligations that arise from allowing it to be shared on the site.

Tildes does allow for topics discussing the subjects of pornography, sexual fetishes, and sexuality, and will likely allow groups to be created in order to facilitate those discussions, but the sharing of pornographic material itself (be it images, videos or fiction) is not allowed.