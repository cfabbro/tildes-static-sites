Title: Features - Mechanics & Design
Slug: feat-mechanics-design
Summary:
Modified: 2018-08-02

[TOC]

This page describes the basic site mechanics and design elements currently on Tildes as well as some potential development paths for their future. None of these mechanics are particularly original since Tildes is intended to be a refinement of community sites not an entirely new type, however some design elements are non-standard for reasons which will be explained.

## Topics

Both external link submissions and user created text posts are referred to as "topics" on Tildes.

Topics can only currently be [sorted](#sorting), [voted](#voting) on by other users ([but only positively](#no-downvoting)), and can be [tagged(#topic-tags) as well as [filtered by tags](#topic-filters), but many more features related to topics are already planned and being considered, such as the ability to post them anonymously via a ["dissociate" (throwaway account-like) mechanic](#throwaway-accounts).

There are some restrictions on what sort of topics can be posted to Tildes, which users should be aware of and are covered in the [FAQ related to Content](/faq-content), the [Code of Conduct](/pol-code-of-conduct), and the ["Content Restrictions"](/pol-terms-of-use#content-restrictions) section of the [Terms of Use](/pol-terms-of-use).

### Sorting

Topics can be sorted using various methods, all of which can be further refined by timespans.  How most sorts and timespans work should be self evident, but "Activity" and "other period" may require some clarification:

The Activity sort functions similar to the "bump" mechanic on forums where, when a new topic is posted it is placed at the top, and when a new comment is made within a topic it will also be "bumped" back to the top as well.  This sorting method allows topics to remain viable and active for far longer than on other aggregators where their short shelf-life often renders making any new comments pointless after activity drops off.  By allowing the revival of older topics via Activity sort on Tildes, users are actually encouraged to fully explore the site since making a new comment on interesting topics they find, regardless of how old they are or their previous activity level, can potentially allow other users to discover that topic and spark a new wave of discussion on it.

The "other period" timespan can be used to set a custom period for the sort and while currently it can only be used to set a specific period in hours or days starting from the current moment, eventually the ability to set custom ranges (to and from) will likely be added as well.

More sorting methods and refinements to the sorting system are likely to be developed for topics as time goes on.

## Comments

All topics on Tildes currently feature a [threaded comment system](https://en.wikipedia.org/wiki/Conversation_threading) in which [threads can be collapsed](#thread-collapsing-folding), [sorted](#sorting_1) and individual comments [voted](#voting) on by other users ([but only positively](#no-downvoting)).  The comment system also includes a "permalink" system so users can easily "permanently link" to specific comments, however it should be noted that its current incarnation is merely a stopgap measure using HTML anchors until a more robust and full featured one can be developed.

Comment tagging was also a feature of the comment system on Tildes, however it was temporarily removed until it can be reworked to address some of the issues that arose with it.  Many more features related to the comment system are also already planned and being considered, such as the ability to post comments anonymously via a ["dissociate" (throwaway account-like) mechanic](#throwaway-accounts).

There are some restrictions on what sort of comments can be posted to Tildes, which users should be aware of and are covered in the [FAQ related to Content](/faq-content), the [Code of Conduct](/pol-code-of-conduct), and the ["Content Restrictions"](/pol-terms-of-use#content-restrictions) section of the [Terms of Use](/pol-terms-of-use).

### Sorting

Currently the comments section can be sorted by 'most votes', 'newest first', and 'order posted', however other types of sorting are being planned and considered.  For example, adding a sort for comments by "activity" where the top-level comments are rearranged based on the reply activity occurring in all the threads under them.

### Thread Collapsing & Folding

Each individual comment thread on Tildes can be "collapsed" by the user in order to help them navigate the comments easier by minimizing threads they have already read or ones they are not interested in, however the state of the collapse is not yet saved and will reset on refreshing the page.  This is only temporary until the collapse save state feature is developed and implemented.

Comment threads can also currently continue forever and too many replies in one continuous chain will break the comment section layout if they do, however this is also only temporary until thread "folding" is added.  This feature will automatically "fold" threads beyond a certain number in a continuous chain into their own discrete sections and those will be then only be accessible via a "continue reading" type link (or some other potential mechanic).  Some other possibilities for the thread folding feature is to have the number of comments in a continuous chain that triggers the folding mechanism determined by the overall size of the comments section in order to help facilitate more top-level comment visibility.  It's also possible that the entire comments section will be folded and auto-collapsed into its own discrete section at the top of the comments section every set number of comments or time period in order to "refresh" the comments section in mega-threads and ongoing events so new top-level comments with updated information have the chance to be seen.

### Comment Box Location

One of the major, non-standard design element choices made on Tildes regarding comments is the new "top-level" (first in a thread) comment box being on the bottom of the page rather than at the top like on other social media sites.  This was done primarily to encourage users to actually read existing comment threads before they make their own new top-level comment, which will hopefully help prevent repetition as well as to encourage existing comment thread participation rather than soap-boxing.  As comment sections grow, the likelihood of new top-level comments being seen diminishes greatly and so too does their value, so the box being pushed further down as the comment section grows is viewed as an inherent feature, not an issue that needs to be "fixed".

Currently all topics have the new comment box at the bottom, however this may not always be the case and certain topics that require encouraging as much top-level participation as possible, such as user feedback threads and "Ask Me Anything" events, will likely be given the option to move the box to the top (or even have it in both places).

## Groups

The various subject-specific communities on Tildes are referred to as "groups", which can be identified by the tilde symbol (`~`) in front of their name.  The reason for this naming convention is that having a unique way to distinguish groups from their subjects (e.g. `~music` vs music) gives users a way to quickly recognize and reference them, and also allows for them to be automatically linked in the comments. There are several compelling reasons for using the tilde symbol to do this:

- It is one of the few ["unreserved"](https://tools.ietf.org/html/rfc3986#section-2.3) URI characters in web addresses.  This means that it is not ["reserved"](https://tools.ietf.org/html/rfc3986#section-2.2) for a specific purpose such as `#`, which is used for HTML anchors, amongst other things. As such, the tilde symbol should always be kept as `~` in the browser address bar since it doesn't require being [percent encoded](https://en.wikipedia.org/wiki/URL-encoding) like reserved characters must be if used for anything other than their established purpose.

- It also has an association with "home" to many technical people since when using the [Bash shell](https://en.wikipedia.org/wiki/Bash_\(Unix_shell\)) in Unix-like operating systems, a tilde is often used to refer to a user's home location. For example, the command `cd ~` navigates to the current user's home directory and `cd ~deimos` will go to the home directory of the user `deimos`.

- Shared hosting platforms on the early Internet often similarly used `~` to distinguish their users' web spaces and many Universities still do. Paul Ford also caused a [fun resurgence of this a few years ago](https://medium.com/message/tilde-club-i-had-a-couple-drinks-and-woke-up-with-1-000-nerds-a8904f0a2ebf) when he started [Tilde.club](http://tilde.club/).

### Hierarchy

Groups on Tildes will be arranged in a hierarchy, meaning they will be organized into a tree-like structure with many branching subgroups under them. There is currently only one subgroup on the site (`~tildes.official`) but eventually all top-level groups will have their own subgroups under them.  For example, the `~music` group could have genre specific subgroups such as `~music.metal` which in turn could have their own subgroups like `~music.metal.instrumental`. This group hierarchy concept should be recognizable to anyone familiar with [Usenet](https://en.wikipedia.org/wiki/Usenet).

### "Ownership"

Unlike on other sites, a group is not "owned" by the user who first created it, but instead ownership is essentially shared amongst all the active users who participate in them.  This will largely be accomplished by the [Trust/Reputation system](/feat-trust-reputation) through which active members of the groups who consistently make valuable contributions to them will gradually earn more rights and powers within that group.

### Creation

Group creation is currently restricted to site administrators but this is likely to change in the future as the site grows.  The reason for this initial restriction is to make it easier to organize the foundation of the hierarchy as well as to concentrate user activity in fewer groups while the site still has a small population. Imzy (a now-defunct social media startup) allowed for users to create groups at launch and as a result hundreds of communities were immediately created for every possible niche subject.  However since there were not enough users to populate these communities, the vast majority of them sat empty and the site, as well as the perception of the site, suffered for it.  Ideally, new groups should only be created organically when there are enough users interested in those subjects to support them and keep them active.
 
Once there are enough users on the site, the current restrictions on group creation can potentially be relaxed and the process largely handed over to the users themselves.  This could take the form of holding regular user polling for new group/subgroup suggestions, where the top result are added for a "trial" phase to see if they are viable (which has already taken place), or perhaps by setting a threshold for trusted user votes required for immediate subgroup creation.  There are many other possibilities for how to approach this and it may even be partially automated based on tag usage where if a subject comes to dominate a group too much, a subgroup is created for that subject automatically.

### Subgroup Interaction

The hierarchical structure of groups on Tildes offers many interesting possibilities for developing systems allowing the various branches to interact with one another. These interactions can prevent subgroups from feeling isolated from each other and also prevent the parent groups from feeling disconnected from all the niche subjects that branch off of them.

One of the major systems being considered to address the issue of parent groups being isolated is allowing well performing topics from subgroups to "bubble up" and appear in their parent groups.  This bubble-up mechanic would allow subscribers of the parent group to still be made aware of important news, events or exemplary topics being posted to the various subgroups. This system would essentially render the users of the subgroups, who are more actively engaged, informed and well versed in that specific niche subject, to act as both meta-curators for the parent group and as moderators for any of the subgroup topics that appear in the parent group.

How best to determine which particular topics from a subgroup will bubble-up to the parent group is still largely undecided but it will likely be some combination of performance based metrics (topic votes, comment volume and votes, etc.). Determining how many of the topics from subgroups bubble-up so as not to overwhelm the parent group is also something that will need to be explored but will likely involve the trusted users of the parent group.  Both how much and what exactly bubbles-up could even potentially be done through a consent/consensus based system where trusted users in subgroups propose particular topics they think should bubble-up and then trusted users in the parent group vote on whether or not to accept and show them.

Another idea being considered for preventing isolation between subgroups is allowing "genuine" cross-posting, where rather than separate instances of a topic being posted individually to various subgroups, it is instead a singular instance mirrored across all the relevant subgroups with a shared comments section.

The beauty of the hierarchy is that many such group interaction systems could eventually be developed for the site and they could even be uniquely designed for particular groups and subgroups to suit their particular needs.

### Custom Behavior

No one system will work best for every community and so each group will potentially be allowed to develop unique tools and systems of their own in order to meet the demands of their specific community, subjects and the topics that they cover. This custom behavior could range from simple restrictions of topic types that can be posted there (e.g. self-text only) to something as complicated as a completely specialized comment sections (e.g. a formal debate system similar to [kialo](https://www.kialo.com/tour/)).  Tildes being open-source even allows for users of the groups to contribute the code for these custom systems themselves so that they can be implemented even if the site's employed developers do not have the time to do so, or the group specific features cannot be made a priority by them.

### Custom CSS / Themes

It is not yet decided if groups will be allowed to implement their own unique custom CSS but if they are, the system will likely be tied to the [Display Theme system](#display-themes) already implemented on Tildes, with administrative oversight provided so that design standards can be enforced in order to help ensure consistency, usability, accessibility and quality of Themes across the site.  These group specific custom themes would also likely be treated merely as suggested defaults for any users visiting the group and would also be simple to enable/disable on a case-by-case or site-wide basis should users desire not to see them.

## User Accounts / Profiles

All registered users of Tildes are given a user profile page which can be accessed by clicking their own username in the top right corner of the site or manually by visiting tildes.net/user/[their username]. When viewing their own profile page, several important features related to a user's account is visible to them:

- Their [Recent Activity / History](#recent-activity-history)
- Notification menu, for viewing any comment replies they receive
- Inbox menu, for viewing private messages
- Invite codes link (which will only show up here if they have received any to give out)
- Settings page, which includes [many other important account features](#settings)
- Log out button

When viewing other user's profile pages, which can be accessed by clicking that user's username anywhere on the site or also manually by visiting tildes.net/user/[their username], that user's [recent activity / history](#recent-activity-history) can be seen, as well as a button to send a Private Message that user.

### Recent Activity / History

While the Recent Activity / History section of the user's profile page is not yet paginated, so there is only a very limited amount of history shown, adding pagination is a high priority and development is [already in progress](https://gitlab.com/tildes/tildes/issues/154).

The current plan for implementing pagination is to only allow users to look through their own full history at first, in order to give them time to go back and edit/delete anything before the paginated history is made visible to every other user.  However, users being able to see the full history of every other user will probably not remain the default forever and it might instead be limited to a set period of time, ideally one long enough that trusted users will be able to adequately detect patterns of behavior (in order to effectively moderate) but not long enough that user's real identities can potentially be compromised or content they posted years ago used against them.  There is also the possibility that the length of user history visible to other users will be tied somehow to the Trust/Reputation system once it is developed, so that only users with a certain level of Trust will be able to see more than a single page.  The length of user history visible to other users may even be set to a minimum length but a setting provided that will allow users to opt-in to allowing a longer period of their history to be seen by others.  Comments and Topic history may also be separated so that all Topics are seen, regardless of how long ago they were made, but Comments limited to a set period.  And when the site goes public (stops requiring an account to view) user profiles are also likely to be accessible only to other logged in users so they cannot be scraped by webcrawlers or caching services.

As with most things on Tildes, the plans for user history are not set in stone and user feedback will be solicited as the feature is developed.

### Settings

The Settings page currently has several important features:

- [Display Theme picker](#display-themes), which allows user to switch between several colour schemes for the site
- A Notification option, which when checked will mark all notifications as read when the Unread Notifications page is viewed
- Open links in new tabs settings
- Password Change option
- Account Recovery, which is incredibly important to set up otherwise a lost password may result in permanent loss of the user account
- Marking New Comments toggle, which allows users to see how many new comments have been made in a topic since they last visited its comment's section
- Topic Tags Filter list, which allow users to [filter out topics by tag](#topic-filters)

Since one of the founding principles of Tildes is to ["let users make their own decisions about what they want to see"](/goals-overall#let-users-make-their-own-decisions-about-what-they-want-to-see) and [privacy as the default setting](/goals-technical#privacy) (meaning all feature with a significant privacy impact should be opt-in), many more feature related settings will likely be added to this page over time.

### Throwaway Accounts

While having [multiple accounts](/code-of-conduct#multiple-accounts) and using one-off "throwaway" ones is not strictly prohibited on Tildes, it's also obviously not ideal, especially with the site currently being invite only.  So as an alternative to these traditional methods for achieving relative anonymity, eventually Tildes may simply allow users to "dissociate" their account from select topics and comments before they post them.  This mechanic would completely remove their username from the selected posts, replacing it with a pseudo-randomly generated identifier that is statically assigned across a topic so that chains of that users "dissociated" replies could still be followed by others without causing confusion.

This mechanic would not be entirely anonymous since the user making the dissociated posts would likely still be tracked internally so that users wouldn't be entirely free from potential consequences should they decide to abuse the mechanic or violate the terms of the site.  However, this internal tracking data would only be available to site administrators (and likely not easily accessible) and so to all outward appearances the user's select topics and/or comments would be entirely anonymous.  And it's also worth mentioning that like most other private/sensitive data on the site, this internal tracking data would likely be purged after 30 days.

### Account/History Deletion

If a user desires to completely delete their account and purge their entire history they can do so by emailing contact@tildes.net and requesting it be done, however this deletion process will be eventually implemented through an account settings option.

The current plan is also to offer users few different options related to this process, in order to allow them to control their data on the site, while also helping mitigate some of the site-wide issues that go along with account/history purging.  These option are:

1. Full account deletion, which completely removes everything (user account and history, including all comments and topics they have made)
2. Full account "dissociation" ([discussed above](#throwaway-accounts)), which leaves the account intact but completely removes the associated username from all comments and topics they have made, replacing it with a pseudo-randomly generated one that is statically assigned across each topic they participated in, so that chains of their replies can still be followed without confusion.

Some other ideas that are also being considered:

- automatically "archiving" topics after X months, where all usernames are replaced with the pseudo-random identifiers (except internally so users can still revisit their old topics/comments in their user history)
- the ability to auto-dissociate user history after X days/months, set by the user
- granular support for topic/comment deletion and dissociation by groups and time ranges (e.g. dissociate all comments made in ~talk between DD/MM/YY and DD/MM/YY)

All these options, when combined with selective comment/topic deletion (which is already implemented) and the planned selective comment/topic dissociation (discussed in the [Throwaway Accounts](#throwaway-accounts) sections), should give users far more options than on most other sites and will hopefully help prevent older topics and comments sections from completely disappearing or becoming entirely useless due to too many totally deleted comments throughout them.

## Voting
 
Each user on Tildes is currently able to vote on comments and topics but other voting mechanisms are likely to be added in the future, such as the ability to vote on tags, in order to determine whether they are appropriate and correctly applied or not.  Currently every user's vote is worth the same across the entire site, however this is also likely to change once the [Trust/Reputation system](/feat-trust-reputation) is developed, as the value of votes for users with more trust in a particular group will likely have significantly more "weight" to them.  This trusted user vote weighting will allow active members of a community to have significantly more say in what content gets visibility there and also help prevent "brigades" (a wave of outside users suddenly entering that group) from having any real negative effect on it as it can even potentially be scaled based on new user influx to that group.
 
### No Downvoting

As mentioned above in several places, Tildes does not have "negative" votes for either topics or comments. The reason for this is it was felt the intended purpose of downvotes and what that intended use accomplishes could be replaced with other features/mechanics instead, without also similarly enabling the misuses of them.
 
The ideal usage of downvotes is to express "this doesn't contribute", but in practice they tend to be used more as a reflexive "I disagree" or "I don't like this" button, regardless of the merits of the content.  This misuse is tremendously counterproductive to quality discussion as it effectively suppresses controversial opinions and exerts peer pressure on users to conform to the prevailing ideology of the often capricious collective (often referred to as the "hivemind"). Posts will also often get downvoted simply because a user doesn't find the content suits their personal taste, and in primarily taste-based communities (such as ones related to music) this can result in entire genres or niches of the group's subject being entirely unviable simply because they will just be downvoted en masse, regardless of their quality.
 
Amongst the features and mechanics meant to replace downvotes on Tildes are comment tags, which can be used to more effectively communicate *why* users think a comment doesn't contribute, and [Topic tags](#topic-tags) which will allow users to simply [filter out](#topic-filters) certain types of posts that don't suit their taste rather than downvoting them.
 
### Exemplary/Super Votes
 
Along with regular votes there is also likely to be an "exemplary" or "super" vote feature on the site for trusted users of the groups that will be in limited supply with new ones given out over time, potentially based on activity and/or tied to the [Trust/Reputation system](/feat-trust-reputation) in some other way.  These alternative votes will likely have significantly more weight to them than normal ones, and feature a visual indication to them in order to further increasing the visibility of topics/comments that users have chosen to "spend" them on.  Ideally these exemplary/super votes will be used to highlight exemplary content within a group, and will also hopefully help exemplary comments, that often take longer to write and so get posted later in the life of a topic, from languishing below the ones that were simply made before them but potentially of less value or quality.

### Vote Button Locations
 
Another of the significant non-standard design choices made on Tildes, besides the placement of the [Comment Box](#comment-box-location), is that of vote button placements.

For comments, the vote button and vote count is below the comments themselves rather than their traditional placement to the left and/or above them. This was done to minimize the negative effects that traditional placement, such as users voting before actually reading a comment fully and to reduce [the bandwagon effect](https://en.wikipedia.org/wiki/Bandwagon_effect).  This placement also helps to deemphasize the vote count and thus the perceived value of votes so that users will hopefully focus more on simply contributing to the discussion rather than attempting to maximize their vote numbers by engineering their comments for maximum appeal.

For topics, the vote button and vote count is to the far right of the topics themselves rather than their traditional placement to the left and/or above them for similar reasons as comments; so that users are encouraged to read the Title and consume the contents of the topic before voting.  This placement is also generally far more convenient for voting on mobile since most user's thumb naturally rests near that position.  However the position of the topic vote button is not seen as nearly as important as the comment vote button so it may be possible to change it to the left side eventually for accessibility's sake.

## Tags & Filters

One of the primary goals of Tildes is to ["let users make their own decisions about what they want to see"](/goals-overall#let-users-make-their-own-decisions-about-what-they-want-to-see) and in order to accomplish that, a comprehensive tagging, filtering, and [search system](#search) will be developed for the site.

A rudimentary form of the [topic tagging](#topic-tags) and [topic filtering](#topic-filters) systems are already implemented, but they are far from feature complete and many more mechanics are likely to be developed for them over time.  A comment tagging system was also implemented but, due to abuse (leading to the site's first account ban), it was temporarily removed until the system could be reworked.

### Topic Tags

In order for users to tag a topic, a field is provided for them in the "Post a new topic" page.  Tags may only be comprised of ASCII letters, numbers, and spaces/underscores (which are synonymous). Some special characters are also allowed; periods which are used as tag hierarchy delimiters, and commas which are used to separate each tag.  There is currently no restriction on the number of tags which can be added to a topic however this may change eventually.  Tags can also be edited after the topic has already been submitted by clicking `tags` on the topic comments section page underneath the topic link or text, and if any edits are made to the tags, they are recorded in the `topic log` shown in the topic info sidebar.

Currently only the user that posted a topic (or an admin) may add and edit tags for that topic, however the plan is to eventually allow other users to be able to do so as well.  This crowd-sourced tagging mechanic will likely be tied in to the [Trust/Reputation system](/feat-trust-reputation) once that is developed, so that users who contribute and edit tags appropriately can earn trust within that group for doing so, and those who abuse the system can be punished with a loss of trust or even have their tagging privileges revoked.

Similar to groups, tags also support a hierarchy which can aid users in refining their searches and filters. For example, topics tagged with `rock.progressive` will still appear in [tag searches](#search) for `?tag=rock`, and should a user desire to filter out progressive rock specifically, or all rock but progressive rock, they may.  It's also worth mentioning that the hierarchy and naming restrictions of topic tags mirrors that of groups in order to keep them compatible with one another.  This compatibility allows for many interesting interactions between them, such as automatically creating a subgroup for a particular tag should it come to dominate a group or folding an inactive subgroup back in to their parent group but adding the subgroup's name as a tag to all the moved topics.

And while tags need to be inputted manually right now, it is possible that in the future certain tags may automatically be added to certain topics, such as a `repost` tag for topics that have been previously submitted, or existing tags automatically modified, such as `breaking.news` turning to just `news` after a specified time period.  Many other similar special cases may also be added eventually to help trusted users manage their groups.

### Topic Filters

A rudimentary topic tag filtering system is already in place and can be found in the user's profile page under `Settings` and `Define topic tags filter`, or it can also be accessed directly by going to [tildes.net/settings/filters](https://tildes.net/settings/filters).

Filters are currently specific, so should a user include `rock` in their filter list then only topics with the `rock` tag would be filtered out but all topics with tags in the hierarchy under rock, such as `rock.progressive`, would still be visible to them.  This means that should a user wish to filter out all rock related topics they would need to include all the "subtags" in the rock hierarchy as well.  This will not always be the case though, as the filter system will likely eventually include more functionality through a query syntax system, including operators such as wildcards (e.g. `*rock.*` to filter out all rock related topics), exceptions (e.g. `*rock.*-progressive` to filter out all but progressive rock), etc.

The filters are also currently global and will be applied to the user's home page as well as inside all group pages, however this will also likely not always be the case and granular support added eventually, especially since trusted users will likely wish to disable them for groups where they are trusted so they can more effectively monitor the activity in that group.

## Search

Development of a basic version of site-wide search is [already in progress](https://gitlab.com/tildes/tildes/issues/90), and while it will likely only include the ability to search for topic titles to start, the intent is to eventually provide a fully featured system capable of searching through the entire site (topic text, comments section, etc) and refining that search by groups, tags, [metadata](#metadata), domains, date/time ranges, etc.

A very basic version of topic tag search is already implemented as well.  Users can click on any topic tag and will be taken to a results page showing all other topics with a matching tag, or they can manually input the tag they wish to search for using the `?tag=` syntax in the address bar.  This can be done on either the main tildes.net page to show site-wide results, or within groups/subgroup pages (e.g. https://tildes.net/~tildes.official?tag=changelog) to show only results within that group. This tag search functionality will also eventually be included in the site-wide search so that users do not have to manually click tags or enter anything in their address bar.

### Metadata

The vast majority of sites on the web include a plethora of ancillary information about themselves and their content which is typically hidden from general view and either buried in the site's code (HTML, XML, JSON, etc) or only accessible via their [API](https://en.wikipedia.org/wiki/Application_programming_interface) (if they have one).  This ancillary information, or "data about data", is known as [metadata](https://en.wikipedia.org/wiki/Metadata#On_the_Internet) and while much of it is only really useful to web browsers and various other web applications, it also often includes information that can actually be rather useful (e.g. keywords, author identity, last modified timestamp, etc), especially to an aggregator such as Tildes.  Many database sites also exist that provide [APIs](https://en.wikipedia.org/wiki/Application_programming_interface) which can be queried so that further metadata can be acquired about a particular subject, topic or piece of media (e.g. Wikipedia, Musicbrainz, thetvdb, etc).  And even where metadata is not officially provided or readily available, it can often be collated from sites by using [web scraping](https://en.wikipedia.org/wiki/Web_scraping) techniques.

Tildes intends to take full advantage of all this available metadata in order to help users refine their searches on the site and even assist them in posting topics, by doing things like suggesting a link's topic title which properly conforms to the group's title standards (e.g. `ArtistName - SongName [album + (year of release)]`) and automatically filling out the topic tags for them by querying relevant databases (e.g. use MusicBrainz API to get a song/artist genre).  This metadata can also be used to help users find other pertinent information or related content by linking it in the Topic info sidebar (e.g. provide links to the primary source for any studies/academic papers, ISBNs for books, etc).

## Display Themes

Tildes supports a basic "display theme" system and has four included themes that completely change the colour scheme of the site.  These themes can be switched between in the `User Profile` - `Settings` page and while the user's choice is currently only saved to a cookie, meaning the setting is not persistent across devices and will be reset if the cookie is deleted, there are [already plans to include it as an internal account setting](https://gitlab.com/tildes/tildes/issues/168) to have it persistent accross devices and also prevent an accidental reset by clearing browser cookies.

The four currently available themes are `White` (which is the default for new users) and `Black`, both of which are custom variants of the other two available themes, `Solarized Light` and `Solarized Dark` respectively.  [The Solarized colour scheme was developed by Ethan Schoonover](http://ethanschoonover.com/solarized) and the it was chosen as the base for the themes on Tildes because it is already popular amongst programmers and has the convenient property of being able to easily flip between dark and light modes.

### User Submitted Themes

Given that Tildes is now open-source and excepting code contributions, it is entirely possible that users will be allowed to submit their own custom themes to include in the native display themes available in the user Settings.  While this is not entirely decided yet, if it is eventually allowed, these user submitted themes would likely be required to meet certain design standards, which would likely include rules such as "colour schemes changes only" with "no relocating features or design elements" (unless given express permission beforehand), but there may be even more restrictions to them in order to ensure the consistency, usability, accessibility and quality of Themes across the site.

### Third-Party Themes & Extensions

While there is already a display theme system included on Tildes, there is also nothing stopping users from actively modifying their own experience on the site using third-party themes and extensions, nor is that even discouraged.  In fact, many user created themes and even a browser extension for the site already exist and can be found at the [awesome-tildes project](https://gitlab.com/Emerald_Knight/awesome-tildes) thanks to the Tildes user @Emerald_Knight who created and maintains it, and @Bauke, @crius and @rndmprsn who have contributed a great deal of work towards developing the various themes and extension.