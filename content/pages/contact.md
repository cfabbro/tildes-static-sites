Title: Contact
Slug: contact
Summary:
Modified: 2018-08-01

Contact information for Tildes.

## Email
**[contact@tildes.net](mailto:contact@tildes.net)** - For general inquiries and all other purposes not listed below.

**[invites@tildes.net](mailto:invites@tildes.net)** - To request an invite to the Tildes alpha.</sup>

**[security@tildes.net](mailto:security@tildes.net)** - To responsibly disclose security related issues.  Tildes does not offer a bug bounty.</sup>

**[donate@tildes.net](mailto:donate@tildes.net)** - For any donation related questions or concerns.

**[press@tildes.net](mailto:press@tildes.net)** - For journalists, bloggers, etc. looking to inquire about Tildes.

**[abuse@tildes.net](mailto:abuse@tildes.net)** - To report copyright infringement or other abuse\* on Tildes.

---

<sup>\*See our [Code of Conduct](https://docs.tildes.net/pol-code-of-conduct) for what constitutes abuse, and the ["Copyright infringement claims"](https://docs.tildes.net/pol-terms-of-use#copyright-infringement-claims) section in our [Terms of Use](https://docs.tildes.net/pol-terms-of-use) for copyright notice requirements and our expected actions in response to receiving one.</sup>

## Other Sites
Reddit - **[/r/tildes](https://old.reddit.com/r/tildes)**  
Twitter - **[@TildesNet](https://twitter.com/TildesNet)**  
Patreon - **[/tildes](https://www.patreon.com/tildes)**  

