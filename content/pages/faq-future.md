Title: FAQ - Future
Slug: faq-future
Summary:
Modified: 2018-08-01

[TOC]

Answers to commonly asked questions about the future of Tildes that are not addressed in [Mechanics & Design](/feat-mechanics-design) or [Trust/Reputation System](/feat-trust-reputation).

## Will Tildes remain invite-only forever?

No. Once the [trust/reputation system](/feat-trust-reputation) is fully implemented and a solid base community of trusted users is established, Tildes will open up registration to the public. However it is also possible that temporary or probationary accounts will be implemented at some point before then and some particular types of topics (such as major public figure "Ask Me Anything" events) might be designed to allow public comments that do not require an account in order to submit them. However this is still an evolving discussion and no decisions have been finalized yet.

## Will Tildes remain private and viewable only to logged-in users forever?

No. At some point Tildes will be publicly viewable although there will likely be some restrictions to it, such as those without accounts not being able to view user profiles or perhaps even specific usernames at all (i.e. only static-per-thread pseudo-random IDs) in order to maintain user privacy.

## Will Tildes ever allow non-English communities?

Probably, but not right now. Many of Tildes goals will be difficult or impossible to work towards without being able to understand what is going on in a community, so for now they need to be primarily in English. However this may change in the future and the [hierarchical groups](/feat-mechanics-design#hierarchy) can even work very well for giving other languages their own unique set of groups. Further down the line, should Tildes gain enough traction and resources to manage them effectively, language specific sub-domains such as de.tildes.net may even be created, but that is likely a long ways off.

## How will Tildes handle bots?

Bots will likely be required to run on special, clearly labelled "bot" accounts so the owners of those accounts and the purpose of the bots can be clearly identified, and in order to give users an easy way to identify and filter them out should they desire not to see them.  Any bots that are found to be running on regular user accounts will likely be met with a ban and bot accounts will likely have slightly restricted access to the site in some ways (e.g. no ability to vote), but also potentially expanded access in other ways via the API.  All bots running on Tildes may also be required to be open-source, so that users can do code review, auditing and collaborate on improving the bot.  Bots that prove themselves invaluable or incredibly useful to particular groups or the site as a whole may even be officially "adopted" in order to integrate them and their functionality to the site as well.

## What if you don't get enough donations to run the site?

One of the luxuries of avoiding venture capital and other forms of investment is that there is no real financial pressure on the site and so Tildes doesn't have to reach certain thresholds of traffic or revenue to prevent shutting down. The worst case scenario is that it ends up as an out-of-pocket side project with the hope of eventually growing to a point where it's sustainable and can be worked on full-time.