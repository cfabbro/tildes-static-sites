Title: FAQ - User Accounts
Slug: faq-user-accounts
Summary:
Modified: 2018-08-01

[TOC]

Answers to Accounts related questions not addressed elsewhere in the documentation.

## I forgot/lost my password, how do I recover my account?

If you set up account recovery properly in your user settings, simply send an email requesting a password reset to [contact@tildes.net](mailto:contact@tildes.net) from the email address associated with your account and instructions for resetting the password will be sent back to it.  If you have multiple accounts associated with the same email address be sure to specify which one you are attempting to recover.

If you did not set up account recovery then it may not be possible for you to recover your account, however you can still email [contact@tildes.net](mailto:contact@tildes.net), explain the situation and something might be able to be done for you.


## Can I register multiple accounts or use "throwaways"?

Yes, as spelled out in the ["Multiple accounts" section](/pol-code-of-conduct#multiple-accounts) of the [Code of Conduct](/pol-code-of-conduct) you may register and use multiple Tildes accounts, however there are some restrictions regarding them that may result account bans if any of the prohibited behavior is detected so it is highly recommended you read the policy documentation regarding them.  Another thing to keep in mind is that multiple accounts may not even be strictly necessary on Tildes if/when [topic and comment "dissociation"](/feat-mechanics-design#throwaway-accounts) is implemented.