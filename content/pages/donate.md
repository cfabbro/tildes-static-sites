Title: $ Donate
Slug: donate
Summary:
Modified: 2018-08-01

[TOC]

Thank you for considering donating to Tildes!  
If you have any questions or concerns, please contact [donate@tildes.net](mailto:donate@tildes.net).

## Why should I donate to Tildes?

Tildes has no investors, no advertising, and does not sell anything (including its users' data). Donations are its only income. By donating, you're supporting a site that has chosen to avoid those other sources of revenue in order to gain the freedom to focus exclusively on acting in its users' interests.

## Who am I donating to?

Tildes is operated by Spectria, a Canadian not-for-profit corporation (corporation number 1034108-8).

A brief bio on Chad Birch, the founder and current Director of Spectria, as well as lead developer of Tildes, can be found in the ["Who's working on Tildes?"](https://blog.tildes.net/announcing-tildes#whos-working-on-tildes-and-how-can-i-contact-you) section of the ["Announcing Tildes" blog post](https://blog.tildes.net/announcing-tildes).

## Important information for donating

+ Tildes uses third-party payment processors and does not handle or have access to any of your sensitive financial information.
+ The corporation that operates Tildes is named "Spectria"&mdash;depending on the donation method you use, you may see this name on the transaction.
+ Spectria is a not-for-profit corporation, but [it is *not* a charity](/faq-about#why-is-tildes-a-non-profit-and-not-a-charity), so donations are not tax-deductible.

## Donation options

### Credit card (via Stripe)

You can donate directly using a credit card through Stripe. Note that the Stripe donation page will load third-party assets from Stripe and communicate with Stripe servers to process the transaction.

[Donate with Stripe](/donate-stripe)

### Patreon

You can set up a recurring monthly donation to Tildes on Patreon. There are no "patron rewards" for using Patreon as of yet, but some may be added later.

[Go to the Patreon page for Tildes](https://patreon.com/tildes)

### Cryptocurrency (via Coinbase)

You can donate Bitcoin (BTC), Bitcoin Cash (BCH), Ethereum (ETH), or Litecoin (LTC) to Tildes via Coinbase.

[Go to the Coinbase donation page for Tildes](https://commerce.coinbase.com/checkout/89160220-7474-4e61-a9fd-b8972a97f9d5)

### Interac e-Transfer

Canadians can donate to Tildes using Interac e-Transfer. Simply send an e-Transfer to donate@tildes.net and it will be auto-accepted (no security question necessary). Note that the recipient will be shown as "SPECTRIA".
