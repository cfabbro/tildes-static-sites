Title: FAQ - About
Slug: faq-about
Summary:
Modified: 2018-08-01

[TOC]

Answers to commonly asked questions about Tildes that are not addressed elsewhere in the site documentation.

## What is Tildes?

Tildes is an [open-source](/goal-technical#license) link aggregator, focusing on facilitating in-depth discussions and the sharing of primarily text-based content.  The organization behind Tildes ([Spectria](/donate#who-am-i-donating-to)) is a Canadian non-profit which is entirely donation driven, so is beholden to no investors or advertisers, only the users of the site itself.

For more detailed information, see the ["Announcing Tildes" blog post](https://blog.tildes.net/announcing-tildes) and the other site documentation.

## Why call it "Tildes"?

The tilde symbol (`~`) prepends group names on the site (e.g. [~music](https://tildes.net/~music)), so calling the host of those groups the plural of that symbol name simply made sense.  As for why groups are prepended with the tildes symbol, see the [Groups section](/feat-mechanics-design#groups) of the [Mechanics & Design page](/feat-mechanics-design).

## Who is working on Tildes?

A brief bio on Chad Birch, the founder and current Director of Spectria, as well as lead developer of Tildes, can be found in the ["Who's working on Tildes?"](https://blog.tildes.net/announcing-tildes#whos-working-on-tildes-and-how-can-i-contact-you) section of the ["Announcing Tildes" blog post](https://blog.tildes.net/announcing-tildes).  And now that the site is open-source there are also many others volunteering their time and contributing [to the site's code](https://gitlab.com/tildes/tildes/commits/master) and [to the site's documentation](https://gitlab.com/tildes/tildes-static-sites/commits/master) as well.

## Why use the .net domain?

Since Tildes is essentially a network of groups and their users, .net seemed the most appropriate top-level domain (TLD) to use.  Tild.es is also controlled by Tildes and will be used for link shortening eventually, as are Tildes.org and Spectria.org which will likely be used specifically for the non-profit organization itself.

## How do you pronounce "Tildes"?

The plural of `/ˈtɪldə/` (sounds like ["til-duh"](https://upload.wikimedia.org/wikipedia/commons/a/a6/En-us-tilde-2.ogg) + "s") is technically the correct pronunciation, however the plural of `/ˈtɪldi/` (sounds like ["til-dee"](https://upload.wikimedia.org/wikipedia/commons/8/86/En-us-tilde.ogg) + "s") is also common and perfectly acceptable as well.

## Why is Tildes a non-profit and not a charity?

Canada only grants charity status to [organizations with certain purposes](https://www.canada.ca/en/revenue-agency/services/charities-giving/giving-charity-information-donors/about-registered-charities/what-difference-between-a-registered-charity-a-non-profit-organization.html). Generally, the organization has to be devoted to relieving poverty, advancing religion or education, or benefit the (local, real-life) community. These are quite restricted definitions and as a result even [Wikimedia Canada](https://ca.wikimedia.org/wiki/About_us) (the Canadian branch of the organization behind Wikipedia) is a non-profit and not a charity. If building Wikipedia doesn't qualify as "advancing education" it's unlikely that Tildes does either.

## Why isn't Tildes decentralized/distributed/federated?

Decentralized communities are interesting and have a lot of potential, but that model also introduces its own unique problems and difficulties. Tildes is already attempting to do quite a few things differently to improve the quality of online communities and interactions, so focusing on achieving those goals needs to take priority and it was felt that introducing the additional complexity of and development time required for decentralization would likely hurt the site more than help it.

However, since Tildes is open-source, someone else could always use it as a base for their own decentralized version.